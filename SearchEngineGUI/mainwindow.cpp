#include <iostream>
#include <QMenu>
#include <QTabBar>
#include <QItemSelectionModel>
#include <QModelIndex>
#include <QAction>
#include <QMenu>
#include <QStringListModel>
#include <QStringList>
#include <QListView>
#include <QFileDialog>
#include <QFile>
#include <QRegularExpression>
#include <QMessageBox>
#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "filelistmodel.h"
#include "tablemodel.h"
#include "InvertedIndex.h"
#include "ConfigException.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{

    settings = new Settings();
    settings->loadSettings();
    ConverterJSON &conv = settings->converter();
    InvertedIndex &index = settings->index();
    SearchServer &server = settings->server();

    ui->setupUi(this);
    ui->lProgramName->setText(settings->name());
    ui->lVersion->setText(settings->version());
    ui->leMaxResponses->setText(QString::number(settings->maxResponses()));
    ui->tabWindow->tabBar()->hide();
    ui->tabWindow->setCurrentIndex(1);
    ui->pbDeleteFiles->setDisabled(true);
    ui->textEdit->setReadOnly(true);
    ui->leConfigPath->setText(settings->configPath());
    ui->leAnswersPath->setText(settings->answerPath());
    ui->leRequestsPath->setText(settings->requestPath());
    ui->leFilesPath->setText(settings->filesFolderPath());

    modelAddFiles = new FileListModel(this);
    ui->lvAddFiles->setModel(modelAddFiles);
    ui->lvAddFiles->setEditTriggers(QAbstractItemView::NoEditTriggers);
    selectionAddFiles = new QItemSelectionModel(modelAddFiles);
    ui->lvAddFiles->setSelectionModel(selectionAddFiles);

    modelRemoveFiles = new FileListModel(this);
    ui->lvRemoveFiles->setModel(modelRemoveFiles);
    ui->lvRemoveFiles->setEditTriggers(QAbstractItemView::NoEditTriggers);
    selectionRemoveFiles = new QItemSelectionModel(modelRemoveFiles);
    ui->lvRemoveFiles->setSelectionModel(selectionRemoveFiles);

    requestModel = new FileListModel(this);
    ui->lvRequest->setModel(requestModel);
    ui->lvRequest->setEditTriggers(QAbstractItemView::NoEditTriggers);
    requestSelection = new QItemSelectionModel(requestModel);
    ui->lvRequest->setSelectionModel(requestSelection);

    answersModel = new TableModel(this);
    ui->tvAnswers->setModel(answersModel);
    ui->tvAnswers->setEditTriggers(QAbstractItemView::NoEditTriggers);

    //добавить файлы кнопкой Add Files
    QObject::connect(ui->pbAddFilesToList, &QPushButton::clicked, this, &MainWindow::addFiles);

    //скопировать файлы из списка в папку resources
    QObject::connect(ui->pbAddFilesToResources, &QPushButton::clicked, this, [this, &conv](){
        QDir filesDir(QString::fromStdString(conv.GetFilesFolderPath()));
        if (!filesDir.exists()) {
            QMessageBox::information(this, "Warning", "Not found folder \"resources\"");
            return;
        }
        for (int i = 0; i < modelAddFiles->rowCount(QModelIndex()); ++i) {
            QModelIndex index = modelAddFiles->index(i, 0, QModelIndex());
            QFileInfo file(modelAddFiles->data(index, Qt::DisplayRole).toString());
            QString dest = filesDir.absolutePath() + "/" + file.fileName();
            QFile::copy(modelAddFiles->data(index, Qt::DisplayRole).toString(), dest);
        }
        modelAddFiles->clearData();
        QMessageBox::information(this, "Sucess", "Files copied to folder \"resources\"");
    });

    //удалить файлы из папки resources
    QObject::connect(ui->pbDeleteResourceFiles, &QPushButton::clicked, this, [this](){
        const QModelIndexList indexes = selectionRemoveFiles->selectedIndexes();
        for (auto &index: indexes) {
            QFile::remove(modelRemoveFiles->data(index, Qt::DisplayRole).toString());
        }
        modelRemoveFiles->deleteValues(selectionRemoveFiles->selection());
    });

    //удалить запросы (delete request)
    QObject::connect(ui->pbDeleteRequest, &QPushButton::clicked, this, [this](){
        requestModel->deleteValues(requestSelection->selection());
    });

    //сохранить запросы в файл requests.json (Save Requests)
    QObject::connect(ui->pbSaveRequests, &QPushButton::clicked, this, [this, &conv](){
        std::vector<std::string> listRequest;
        for (int i = 0; i < requestModel->rowCount(QModelIndex()); ++i) {
            QModelIndex index = requestModel->index(i, 0, QModelIndex());
            QVariant request = requestModel->data(index, Qt::DisplayRole);
            listRequest.push_back(request.toString().toStdString());
        }
        try {
            if (conv.SetRequests(listRequest)) {
                QMessageBox::information(this, "Sucess", "Request saved");
            } else {
                QMessageBox::information(this, "Error", "Request not saved");
            }
        } catch (const ConfigException &ex) {
            QMessageBox::information(this, "Error", QString::fromStdString(ex.getMessage()));
        }
    });

    //сохранить список файлов в config.json (Save to Config)
    QObject::connect(ui->pbSaveToConfig, &QPushButton::clicked, this, [this, &conv](){
        QDir filesDir(QString::fromStdString(conv.GetFilesFolderPath()));
        if (!filesDir.exists()) {
            QMessageBox::information(this, "Error", "Not found folder \"resources\"");
            return;
        }

        std::vector<std::string> outputFiles;
        std::vector<QString> inputFiles;
        for (int i = 0; i < modelRemoveFiles->rowCount(QModelIndex()); ++i) {
            QModelIndex index = modelRemoveFiles->index(i, 0, QModelIndex());
            QVariant filePath = modelRemoveFiles->data(index, Qt::DisplayRole);
            inputFiles.push_back(filePath.toString());
        }

        for (auto filePath: inputFiles) {
            QFileInfo file(filePath);
            QString fileName(file.fileName());
            QString outputFilePath(filesDir.filePath(fileName));
            outputFiles.push_back(outputFilePath.toStdString());
            QFile::copy(filePath, outputFilePath);
        }

        if (conv.WriteFilesToConfig(outputFiles)) {
            QMessageBox::information(this, "Sucess", "Filelist saved");
        } else {
            QMessageBox::information(this, "Error", "Filelist not saved");
        }
    });

    //получение пути к файлам в settings
    QObject::connect(ui->pbSetFilesPath, &QPushButton::clicked, this, [this, &conv](){
        QString path = QFileDialog::getExistingDirectory();
        conv.SetFilesFolderPath(path.toStdString());
        settings->setFilesFolderPath(path);
        ui->leFilesPath->setText(path);
    });
    //получение пути к файлу answers.json
    QObject::connect(ui->pbSetAnswersPath, &QPushButton::clicked, this, [this, &conv](){
        QString path = QFileDialog::getOpenFileName(this,"", QDir::current().absolutePath(), "answers.json", nullptr);
        conv.SetAnswersPath(path.toStdString());
        settings->setAnswerPath(path);
        ui->leAnswersPath->setText(path);
    });
    //получение пути к файлу config.json
    QObject::connect(ui->pbSetConfigPath, &QPushButton::clicked, this, [this, &conv](){
        QString path = QFileDialog::getOpenFileName(this,"", QDir::current().absolutePath(), "config.json", nullptr);
        conv.SetConfigPath(path.toStdString());
        settings->setConfigPath(path);
        ui->leConfigPath->setText(path);
    });
    //получение пути к файлу requests.json
    QObject::connect(ui->pbSetRequestsPath, &QPushButton::clicked, this, [this, &conv](){
        QString path = QFileDialog::getOpenFileName(this,"", QDir::current().absolutePath(), "requests.json", nullptr);
        conv.SetRequestsPath(path.toStdString());
        settings->setRequestPath(path);
        ui->leRequestsPath->setText(path);
    });
    //сохранение путей в settings
    QObject::connect(ui->pbSavePaths, &QPushButton::clicked, this, [this, &conv](){
        settings->saveSettings();
        QMessageBox::information(this, "Sucess", "Settings saved");
    });

    //отключение-включение кнопки Delete при выделении, вывод текста файла при единичном выделении
    QObject::connect(selectionAddFiles, &QItemSelectionModel::selectionChanged, this, [this](){
        if (selectionAddFiles->hasSelection()) {
            ui->pbDeleteFiles->setDisabled(false);
        } else {
            ui->pbDeleteFiles->setDisabled(true);
            return;
        }
        QString text;
        QFile file(modelAddFiles->selectedRowData(selectionAddFiles->selection()).toString());
        if (file.exists()) {
            if (file.open(QIODevice::ReadOnly)) {
                 QTextStream stream(&file);
                 text = stream.readAll();
                 file.close();
            }
        }
        ui->textEdit->setText(text);
    });
    QObject::connect(selectionRemoveFiles, &QItemSelectionModel::selectionChanged, this, [this](){
        if (selectionRemoveFiles->hasSelection()) {
            ui->pbDeleteResourceFiles->setDisabled(false);
        } else {
            ui->pbDeleteResourceFiles->setDisabled(true);
            return;
        }
        QString text;
        QFile file(modelRemoveFiles->selectedRowData(selectionRemoveFiles->selection()).toString());
        if (file.exists()) {
            if (file.open(QIODevice::ReadOnly)) {
                 QTextStream stream(&file);
                 text = stream.readAll();
                 file.close();
            }
        }
        ui->textEdit->setText(text);
    });
    // кнопка Cancel в меню Paths
    QObject::connect(ui->pbCancelPaths, &QPushButton::clicked, this, [this](){
        ui->tabWindow->setCurrentIndex(0);
    });
    // кнопка Cancel в меню Settings
    QObject::connect(ui->pbCancelSettings, &QPushButton::clicked, this, [this](){
        ui->tabWindow->setCurrentIndex(0);
    });
    // нормализация текста (кнопка Normalise)
    QObject::connect(ui->pbNormalize, &QPushButton::clicked, this, [this](){
        QFile file(modelRemoveFiles->selectedRowData(selectionRemoveFiles->selection()).toString());
        if (file.exists()) {
            if (file.open(QIODevice::ReadWrite)) {
                 QTextStream stream(&file);
                 QString text = stream.readAll();
                 std::string str = text.toStdString();
                 str = settings->converter().NormalizeDocument(str);
                 text = QString::fromStdString(str);
                 file.resize(0);
                 file.write(text.toStdString().c_str());
                 file.close();
            }
        }

    });

    //кнопка добавления запроса Add Request
    QObject::connect(ui->pbAddRequest, &QPushButton::clicked, this, &MainWindow::addRequest);

    //добавление запроса нажатием клавиши Enter
    QObject::connect(ui->leRequest, &QLineEdit::returnPressed, this, &MainWindow::addRequest);

    //кнопка получения ответов Get Ansvers
    QObject::connect(ui->pbGetAnswers, &QPushButton::clicked, this, [this, &conv](){
        std::vector<Answer> answers;
        try {
            answers = conv.GetAnswers();
            for (auto &answer: answers) {
                answersModel->addValue(answer);
            }
        } catch (const ConfigException &ex) {
            QMessageBox::information(this, "Error", QString::fromStdString(ex.getMessage()));
        }
    });

    //действия из меню
    QObject::connect(ui->action_Exit, &QAction::triggered, this, [this](){
        this->close();
    });
    QObject::connect(ui->action_Requests, &QAction::triggered, this, [this](){
        ui->tabWindow->setCurrentIndex(2);
        ui->leRequest->setFocus();
    });
    QObject::connect(ui->action_Answers, &QAction::triggered, this, [this](){
       ui->tabWindow->setCurrentIndex(3);
    });
    QObject::connect(ui->action_Paths, &QAction::triggered, this, [this](){
        ui->tabWindow->setCurrentIndex(4);
    });
    QObject::connect(ui->action_Config, &QAction::triggered, this, [this](){
        ui->tabWindow->setCurrentIndex(5);
    });
    QObject::connect(ui->action_Help, &QAction::triggered, this, [this](){
        ui->tabWindow->setCurrentIndex(6);
    });
    QObject::connect(ui->action_About, &QAction::triggered, this, [this](){
        ui->tabWindow->setCurrentIndex(7);
    });
    QObject::connect(ui->action_RemoveFiles, &QAction::triggered, this, [this, &conv](){
        ui->tabWindow->setCurrentIndex(0);
        readResourceFiles(conv);
    });
    QObject::connect(ui->action_AddFiles, &QAction::triggered, this, [this, &conv](){
        ui->tabWindow->setCurrentIndex(1);
    });

}

Ui::MainWindow* MainWindow::getUI() {
    return ui;
}

MainWindow::~MainWindow()
{
    delete settings;
    delete ui;
}

void MainWindow::readResourceFiles(const ConverterJSON& conv)
{
    QDir filesDir(QString::fromStdString(conv.GetFilesFolderPath()));
    if (!filesDir.exists()) {
        QMessageBox::information(this, "Warning", "Not found folder \"resources\"");
        return;
    }
    QStringList filters("*.txt");
    QFileInfoList filesList(filesDir.entryInfoList(filters, QDir::Files));
    if (!filesList.empty()) {
        modelRemoveFiles->clearData();
        for (auto filePath: filesList) {
            modelRemoveFiles->addValue(filePath.absoluteFilePath());
        }
    }
}

void MainWindow::addRequest()
{
    QStringList list = ui->leRequest->text().split(QRegularExpression("\\s+"));
    for (const auto &word: list) {
        if (!QRegularExpression("(^[a-z]+$)|(^[0-9]+$)|(^[a-z]+-[a-z]+$)").match(word).hasMatch()) {
            QMessageBox::information(this, "Error", "Wrong request!");
            ui->leRequest->setFocus();
            return;
        }
    }
    requestModel->addValue(ui->leRequest->text());
    ui->leRequest->clear();
    ui->leRequest->setFocus();
}

void MainWindow::addFiles()
{
    QStringList fileList = QFileDialog::getOpenFileNames(this,"", QDir::current().absolutePath(), "*.txt", nullptr);
    for (QString &path: fileList) {
        modelAddFiles->addValue(path);
    }
}

